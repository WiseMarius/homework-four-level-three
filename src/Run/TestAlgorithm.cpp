#include "TestAlgorithm.h"

TestAlgorithm::TestAlgorithm()
{
	srand(time(0));

	std::string customerName;
	std::string cardId;
	std::string transactionName;
	Customer *customer;
	DiscountRate *discount = new DiscountRate();
	Visit *visit;
	float totalPrice = 0;
	std::map<std::string, std::shared_ptr<CreditCard>>::iterator it;

	for (int index = 0; index < 10; index++)
	{
		transactionName = GenerateInput::generateServiceOrProduct();
		customerName = GenerateInput::generateRandomName();
		cardId = GenerateInput::generateRandomString();
		customer = GenerateInput::generateUser(customerName);
		it = visits.find(customerName);
		std::shared_ptr<Money> customerCharge(new Money);
		std::shared_ptr<CreditCard> customerCreditCard(new CreditCard);

		visit = new Visit(customerName, GenerateInput::generateRandomDate());

		for (int index = 0; index < rand() % 10 + 1; index++)
		{
			if (rand() % 2 == 1)
			{
				visit->setProductExpense(visit->getProductExpense() + rand() % 100 + 5);
			}
			else
			{
				visit->setServiceExpense(visit->getProductExpense() + rand() % 100 + 5);
			}
		}

		totalPrice = visit->getProductExpense() + visit->getServiceExpense();
		totalPrice -= discount->getProductDiscountRate(customer->getMemberType()) * visit->getProductExpense();
		totalPrice -= discount->getServiceDiscountRate(customer->getMemberType()) * visit->getServiceExpense();
		customerCharge = std::shared_ptr<Money>(new Money((int)totalPrice, (int)((totalPrice - (int)totalPrice) * 100)));

		customerCreditCard->setOwnerName(customerName);
		customerCreditCard->setCardNumber(cardId);
		customerCreditCard->chargeMethod(transactionName, customerCharge);

		visits[customerName] = customerCreditCard;
	}
}

void TestAlgorithm::displayVisits()
{
	for (std::map<std::string, std::shared_ptr<CreditCard>>::iterator index = this->visits.begin(); index != this->visits.end(); ++index)
	{
		std::cout << "Clientul " << index->first << " are urmatoarele facturui: " << std::endl;
		index->second->printTransactions();
	}
}