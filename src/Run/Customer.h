#pragma once

#include <iostream>
#include <string>

class Customer
{
private:
	std::string name;
	bool member = false;
	std::string memberType;

public:
	Customer(std::string pName);

	friend std::ostream &operator<<(std::ostream &output, const Customer &pCustomer);

	void setName(std::string pName);
	std::string getName();
	void setMember(bool isMember);
	bool isMember();
	void setMemberType(std::string pSetMemberType);
	std::string getMemberType();
};