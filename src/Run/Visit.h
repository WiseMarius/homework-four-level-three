#pragma once

#include <iostream>
#include <ctime>

#include "Customer.h"

class Visit
{
private:
	Customer *customer;
	tm *date;
	double serviceExpense;
	double producExpense;

public:
	Visit(std::string pName, tm *pDate);

	friend std::ostream &operator<<(std::ostream &output, const Visit &pVisit);

	std::string getName();
	tm getDate();
	void setDate(tm *pDate);
	double getServiceExpense();
	void setServiceExpense(double pServiceExpense);
	double getProductExpense();
	void setProductExpense(double pProductExpense);
};