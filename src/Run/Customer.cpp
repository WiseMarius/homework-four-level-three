#include "Customer.h"

Customer::Customer(std::string pName)
{
	this->name = pName;
}

void Customer::setName(std::string pName)
{
	this->name = pName;
}

std::string Customer::getName()
{
	return this->name;
}

void Customer::setMember(bool isMember)
{
	this->member = isMember;
}

bool Customer::isMember()
{
	return this->member;
}

void Customer::setMemberType(std::string pMemberType)
{
	this->memberType = pMemberType;
}

std::string Customer::getMemberType()
{
	return this->memberType;
}

std::ostream & operator<<(std::ostream & output, const Customer & pCustomer)
{
	output << pCustomer.name;

	return output;
}
