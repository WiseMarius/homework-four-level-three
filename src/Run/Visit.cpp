#include "Visit.h"

Visit::Visit(std::string pName, tm *pDate)
{
	this->customer = new Customer(pName);
	this->date = pDate;
	this->producExpense = 0;
	this->serviceExpense = 0;
}

std::string Visit::getName()
{
	return this->customer->getName();
}

tm Visit::getDate()
{
	return tm();
}

void Visit::setDate(tm *pDate)
{
	this->date = pDate;
}

double Visit::getServiceExpense()
{
	return this->serviceExpense;
}

void Visit::setServiceExpense(double pServiceExpense)
{
	this->serviceExpense = pServiceExpense;
}

double Visit::getProductExpense()
{
	return this->producExpense;
}

void Visit::setProductExpense(double pProductExpense)
{
	this->producExpense = pProductExpense;
}

std::ostream &operator<<(std::ostream &output, const Visit &pVisit) 
{
	output << pVisit.customer->getName() << " visited us on " << pVisit.date->tm_mday << "." << pVisit.date->tm_mon << " at " << pVisit.date->tm_hour << ":" << pVisit.date->tm_min;

	return output;
}
