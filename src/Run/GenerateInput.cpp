#include <string>

#include "GenerateInput.h"

const std::string userTypes[] = { "Premium", "Gold", "Silver" };

std::string GenerateInput::generateRandomString()
{
	static const char alphanum[] =
		"0123456789"
		"ABCDEFGHIJKLMNOPQRSTUVWXYZ";

	std::string transactionName;

	for (int index = 0; index < 10; index++)
	{
		transactionName += alphanum[rand() % (sizeof(alphanum) - 1)];
	}

	return transactionName;
}

std::string GenerateInput::generateRandomName()
{
	return "Name" + std::to_string(rand() % 10 + 1);
}

tm* GenerateInput::generateRandomDate()
{
	tm *randomDate = new tm();
	std::string dateString;

	randomDate->tm_year = rand() % 2000 + 2016;
	randomDate->tm_mon = rand() % 12 + 1;
	randomDate->tm_mday = rand() % 31 + 1;

	return randomDate;
}

Customer * GenerateInput::generateUser(std::string userName)
{
	Customer *customer = new Customer(userName);

	customer->setMember(rand() % 1);
	
	if (customer->isMember())
	{
		customer->setMemberType(userTypes[rand() % 3 + 1]);
	}

	return customer;
}

std::string GenerateInput::generateServiceOrProduct()
{
	std::string type;

	if (rand() % 2 == 1)
	{
		type = "SERVICE" + std::to_string(rand() % 10 + 1);
	}
	else
	{
		type = "PRODUCT" + std::to_string(rand() % 10 + 1);
	}

	return type;
}
