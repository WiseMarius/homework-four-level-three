#pragma once

#include "DiscountRate.h"

double DiscountRate::getServiceDiscountRate(std::string pType)
{
	if (pType == "Gold")
	{
		return this->serviceDiscoutPremium;
	}
	else if (pType == "Silver")
	{
		return this->serviceDiscoutSilver;
	}
	else
	{
		return this->serviceDiscoutPremium;
	}
}

double DiscountRate::getProductDiscountRate(std::string pType)
{
	if (pType == "Gold")
	{
		return this->productDiscountGold;
	}
	else if (pType == "Silver")
	{
		return this->productDiscountSilver;
	}
	else
	{
		return this->productDiscountPremium;
	}
}
