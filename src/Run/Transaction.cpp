#include "Transaction.h"

Transaction::Transaction(std::string pName, std::shared_ptr<Money> pCost)
{
	this->name = pName;
	this->cost = pCost;
}

Transaction::Transaction(const Transaction& pTransaction)
{
	this->name = pTransaction.name;
	this->cost = pTransaction.cost;
}

std::ostream &operator<<(std::ostream &output, const Transaction &pTransaction)
{
	output << "Transaction name " << pTransaction.name << "; Transaction cost: " << *pTransaction.cost;

	return output;
}

std::string Transaction::getName()
{
	return this->name;
}
void Transaction::setName(std::string pName)
{
	this->name = pName;
}
std::shared_ptr<Money> Transaction::getCost()
{
	return this->cost;
}
void Transaction::setCost(std::shared_ptr<Money> pCost)
{
	this->cost = pCost;
}