#pragma once

#include <iostream>
#include <string>

#include "Customer.h"

class GenerateInput
{
public:
	static std::string generateRandomString();
	static std::string generateRandomName();
	static tm* generateRandomDate();
	static Customer* generateUser(std::string userName);
	static std::string generateServiceOrProduct();
};