#include <iostream>
#include <map>
#include <string.h>
#include <time.h>
#include <memory>

#include "GenerateInput.h"
#include "Visit.h"
#include "CreditCard.h"
#include "Customer.h"
#include "Money.h"
#include "DiscountRate.h"

class TestAlgorithm
{
private:
	std::map<std::string, std::shared_ptr<CreditCard>> visits;

public:
	TestAlgorithm();

	void displayVisits();
};