#pragma once

#include <iostream>

class DiscountRate
{
private:
	const double serviceDiscoutPremium = 0.2;
	const double serviceDiscoutGold = 0.15;
	const double serviceDiscoutSilver = 0.1;
	double productDiscountPremium = 0.1;
	double productDiscountGold = 0.1;
	double productDiscountSilver = 0.1;

public:
	double getServiceDiscountRate(std::string pType);
	double getProductDiscountRate(std::string pType);
};