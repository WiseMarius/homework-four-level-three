#pragma once
#include <string>
#include <memory>

#include "Money.h"

class Transaction
{
private:
	std::string name;
	std::shared_ptr<Money> cost;

public:
	Transaction(std::string pName, std::shared_ptr<Money> pCost);
	Transaction(const Transaction &pTransaction);

	friend std::ostream &operator<<(std::ostream &output, const Transaction &pTransaction);

	std::string getName();
	void setName(std::string pName);
	std::shared_ptr<Money> getCost();
	void setCost(std::shared_ptr<Money> pCost);
};